#!/usr/bin/env sh
set -eu
apk add --no-cache py-pip bash

pip install --no-cache-dir docker-compose
